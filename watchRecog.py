import numpy as np
import cv2

watch_cascade = cv2.CascadeClassifier('watchCascade.xml')

cap = cv2.VideoCapture(0)

font = cv2.FONT_HERSHEY_SIMPLEX


while 1:
    ret, img = cap.read()
    img = cv2.flip(img,1)

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    watches = watch_cascade.detectMultiScale(gray, 1.7, 6)

    for (x,y,w,h) in watches:

        cv2.rectangle(img,(x,y),(x+w,y+h),(255,255,0),2)
        
        d = 2860/h
        distance = "dist:"+str(int(d))
        
        cv2.putText(img,distance,(x+h,y+h), font, 0.5, (255,255,255), 2, cv2.LINE_AA)    
 
#        print (distance)

    cv2.imshow('img',img)

    k = cv2.waitKey(30) & 0xff
                   
    if k == 27:
        break

cap.release()
cv2.destroyAllWindows()