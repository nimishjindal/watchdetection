import urllib.request
import cv2
import numpy as np
import os

def create_DataFile(file_type = 'neg'):

    for img in os.listdir(file_type):
        
        line = file_type+'/'+img+'\n'
        with open('bg.txt','a') as f:
            f.write(line)

def store_raw_images():
    neg_images_link = 'http://www.image-net.org/api/text/imagenet.synset.geturls?wnid=n07942152'   
    neg_image_urls = urllib.request.urlopen(neg_images_link).read().decode()
    pic_num = 1
    
    if not os.path.exists('neg'):
        os.makedirs('neg')
        
    for i in neg_image_urls.split('\n'):
        try:
            print(i)
            urllib.request.urlretrieve(i, "neg/"+str(pic_num)+".jpg")
            img = cv2.imread("neg/"+str(pic_num)+".jpg",cv2.IMREAD_GRAYSCALE)
            resized_image = cv2.resize(img, (100, 100))
            cv2.imwrite("neg/"+str(pic_num)+".jpg",resized_image)
            pic_num += 1
            
        except Exception as e:
            print(str(e))
            
    neg_images_link = 'http://www.image-net.org/api/text/imagenet.synset.geturls?wnid=n00523513'   
    neg_image_urls = urllib.request.urlopen(neg_images_link).read().decode()
#    pic_num = 1
    
    if not os.path.exists('neg'):
        os.makedirs('neg')
        
    for i in neg_image_urls.split('\n'):
        try:
            print(i)
            urllib.request.urlretrieve(i, "neg/"+str(pic_num)+".jpg")
            img = cv2.imread("neg/"+str(pic_num)+".jpg",cv2.IMREAD_GRAYSCALE)
            resized_image = cv2.resize(img, (100, 100))
            cv2.imwrite("neg/"+str(pic_num)+".jpg",resized_image)
            pic_num += 1
            
        except Exception as e:
            print(str(e))
  
    
def removeCrap():
        
    match = False
    
    for img in os.listdir('neg'):
        for ugly in os.listdir('uglies'):
            try:
                current_image = 'neg/'+str(img)
                ugly = cv2.imread('uglies/'+str(ugly))
                question = cv2.imread(current_image)
                if ugly.shape == question.shape and not(np.bitwise_xor(ugly,question).any()):
                    print('Deleting!')
                    print(current_image)
                    os.remove(current_image)
            except Exception as e:
                print(str(e))
        
def prepPosImg(imName):

    img = cv2.imread(imName,cv2.IMREAD_GRAYSCALE)
    resized_image = cv2.resize(img, (50, 50))
    cv2.imwrite("iwatch5050.jpg",resized_image)
    
#store_raw_images()
#removeCrap()
#create_DataFile()
prepPosImg("pebble.jpg")
